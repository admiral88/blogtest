﻿$(document).ready(function () {
    $("#submit").click(function (e) {
        if ($("input[name=Type]:checked").val() == "JSON")
        {
            readJson();
        }
        if ($("input[name=Type]:checked").val() == "XML")
        {
            readXml();
        }
    });

    $("#save").click(function (e) {
        if ($("input[name=TypeSave]:checked").val() == "JSON") {
            saveJson();
        }
        if ($("input[name=TypeSave]:checked").val() == "XML") {
            saveXml();
        }
    });
});
function readJson()
{
    $.ajax({
        type: "POST",
        url: "/Home/ReadJSON",
        contentType: "application/json; charset=utf-8",
        success: function (result, status, xhr) {
            $("#booktable").children().not('#thead').remove();
            $("#thead").after(result);
        },
        error: function (xhr, status, error) {
            $("#dataDiv").html("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
        }
    });
}

function readXml()
{
    $.ajax({
        type: "POST",
        url: "/Home/ReadXML",
        contentType: "application/json; charset=utf-8",
        success: function (result, status, xhr) {
            $("#booktable").children().not('#thead').remove();
            $("#thead").after(result);
        },
        error: function (xhr, status, error) {
            $("#dataDiv").html("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
        }
    });
}

function saveJson() {
    var books = [];

    var selectedRows = $('#btable').find('tbody') 
        .find('tr') 
        .has('input[type=checkbox]:checked');

    for (var i = 0; i < selectedRows.length; i++) {
        books[i] = {
            'Id': selectedRows[i].cells.namedItem("id") != null ? selectedRows[i].cells.namedItem("id").innerHTML : " ",
            'Author': selectedRows[i].cells.namedItem("Author") != undefined ? selectedRows[i].cells.namedItem("Author").innerHTML : " ",
            'Name': selectedRows[i].cells.namedItem("Name").innerHTML,
            'DateFrom': selectedRows[i].cells.namedItem("DateFrom").innerHTML,
            'Genre': selectedRows[i].cells.namedItem("Genre").innerHTML,
            'Price': selectedRows[i].cells.namedItem("Price").innerHTML
        }
    }
    var dataObject = JSON.stringify(books);

    $.ajax({
        type: "POST",
        url: "/Home/saveJson",
        data: dataObject,
        dataType: "html",  
        contentType: "application/json; charset=utf-8",
        success: function (result, status, xhr) {
            $("#result").html("JSON Save - " + xhr.statusText);
            setTimeout(clearResult, 2000);
        },
        error: function (xhr, status, error) {
            $("#result").html("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);
            setTimeout(clearResult, 2000);
        }
    });
    
}

function saveXml() {
    var books = [];

    var selectedRows = $('#btable').find('tbody')
        .find('tr')
        .has('input[type=checkbox]:checked');
    for (var i = 0; i < selectedRows.length; i++) {
        //console.log(selectedRows[i].cells.namedItem("id"));
        books[i] = {
            'Id': selectedRows[i].cells.namedItem("id") != null ? selectedRows[i].cells.namedItem("id").innerHTML : " ",
            'Author': selectedRows[i].cells.namedItem("Author") != undefined ? selectedRows[i].cells.namedItem("Author").innerHTML : " ",
            'Name': selectedRows[i].cells.namedItem("Name").innerHTML,
            'DateFrom': selectedRows[i].cells.namedItem("DateFrom").innerHTML,
            'Genre': selectedRows[i].cells.namedItem("Genre").innerHTML,
            'Price': selectedRows[i].cells.namedItem("Price").innerHTML
        }
    }

    var dataObject = JSON.stringify(books);

    $.ajax({
        type: "POST",
        url: "/Home/saveXml",
        data: dataObject,
        dataType: "html",
        contentType: "application/json; charset=utf-8",
        success: function (result, status, xhr) {
            $("#result").html("XML Save - " + xhr.statusText);
            setTimeout(clearResult, 2000);
        },
        error: function (xhr, status, error) {
            $("#result").html("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);
            setTimeout(clearResult, 5000);
        }
    });
}
function clearResult()
{
    $("#result").html("");
}

