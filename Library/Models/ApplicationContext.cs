﻿using Library.BLL.Models;
using Microsoft.AspNet.Identity.EntityFramework;


namespace Library.Models
{
  public class ApplicationContext : IdentityDbContext<ApplicationUser>
  {
    public ApplicationContext() : base("DbConnection") { }

    public static ApplicationContext Create()
    {
      return new ApplicationContext();
    }

    public System.Data.Entity.DbSet<ViewModel> ViewModels { get; set; }
  }
}