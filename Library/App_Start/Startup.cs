﻿using Library.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace Library.App_Start
{
  public class Startup
  {
    public void Configuration (IAppBuilder app)
    {
      app.CreatePerOwinContext<ApplicationContext>(ApplicationContext.Create);
      app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
      app.UseCookieAuthentication(new CookieAuthenticationOptions
      {
        AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
        LoginPath = new Microsoft.Owin.PathString("/Account/Login"),
      });
    }
  }
}