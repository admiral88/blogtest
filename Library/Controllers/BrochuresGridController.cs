﻿using System;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BLL.Services;
using Library.BLL.Models;

namespace Library.Controllers
{
  public class BrochuresGridController : Controller
  {
    private BrochureService _brochuresService = new BrochureService();

    public ActionResult Index()
    {
      return View();
    }

    public ActionResult Brochures_Read([DataSourceRequest]DataSourceRequest request)
    {
      var brochures = _brochuresService.GetBrochures();
      DataSourceResult result = brochures.ToDataSourceResult(request);
      return Json(result);
    }

    [AcceptVerbs(HttpVerbs.Post)]
    public ActionResult Brochures_Create([DataSourceRequest]DataSourceRequest request, BrochureViewModel brochure)
    {
      if (ModelState.IsValid)
      {
        _brochuresService.Create(brochure);
      }
      return Json(new[] { brochure }.ToDataSourceResult(request, ModelState));
    }

    [AcceptVerbs(HttpVerbs.Post)]
    public ActionResult Brochures_Update([DataSourceRequest]DataSourceRequest request, BrochureViewModel brochure)
    {
      if (ModelState.IsValid)
      {
        _brochuresService.Update(brochure);
      }
      return Json(new[] { brochure }.ToDataSourceResult(request, ModelState));
    }

    [AcceptVerbs(HttpVerbs.Post)]
    public ActionResult Brochures_Destroy([DataSourceRequest]DataSourceRequest request, BrochureViewModel brochure)
    {
      if (ModelState.IsValid)
      {
        _brochuresService.Delete(brochure.Id);
      }
      return Json(new[] { brochure }.ToDataSourceResult(request, ModelState));
    }

    [HttpPost]
    public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
    {
      var fileContents = Convert.FromBase64String(base64);

      return File(fileContents, contentType, fileName);
    }

    [HttpPost]
    public ActionResult Pdf_Export_Save(string contentType, string base64, string fileName)
    {
      var fileContents = Convert.FromBase64String(base64);

      return File(fileContents, contentType, fileName);
    }
  }
}
