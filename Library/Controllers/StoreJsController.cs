﻿using System;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BLL.Services;

namespace Library.Controllers
{
  public class StoreJsController : Controller
  {
    private StoreService _libService = new StoreService();

    public ActionResult Index()
    {
      return View();
    }

    public ActionResult ViewModels_Read([DataSourceRequest]DataSourceRequest request)
    {
      var result = new DataSourceResult();
      var listViewModel = _libService.GetViewModel();
      result = listViewModel.ToDataSourceResult(request);
      return Json(result);
    }

    [HttpPost]
    public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
    {
      var fileContents = Convert.FromBase64String(base64);

      return File(fileContents, contentType, fileName);
    }

    [HttpPost]
    public ActionResult Pdf_Export_Save(string contentType, string base64, string fileName)
    {
      var fileContents = Convert.FromBase64String(base64);

      return File(fileContents, contentType, fileName);
    }
  }
}
