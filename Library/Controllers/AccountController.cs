﻿using Library.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
  public class AccountController : Controller
  {
    private ApplicationUserManager UserManager
    {
      get
      {
        return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
      }
    }

    public ActionResult Register()
    {
      return View();
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Register(RegisterModel model)
    {
        if (ModelState.IsValid)
        {
          var user = new ApplicationUser { UserName = model.Email, Email = model.Email, Year = model.Year };
          var result = await UserManager.CreateAsync(user, model.Password);
          if (result.Succeeded)
          {
            await UserManager.AddToRoleAsync(user.Id, "user");
            return RedirectToAction("Index", "StoreJs");
          }
        }
        return View(model);
    }

/// <summary>
/// ////////////////////////////////////////////////////////////////////////////
/// </summary>
    public ActionResult RegisterRole()
    {
      if (ModelState.IsValid)
      {
        var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(ApplicationContext.Create()));
        var role1 = new IdentityRole { Name = "user" };
        roleManager.Create(role1);
      }
      return RedirectToAction("Login", "Account");
    }


    private IAuthenticationManager AuthenticationManager
    {
      get
      {
        return HttpContext.GetOwinContext().Authentication;
      }
    }

    public ActionResult Login(string returnUrl)
    {
      ViewBag.returnUrl = returnUrl;
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Login(LoginModel model, string returnUrl)
    {
      if (ModelState.IsValid)
      {
        ApplicationUser user = await UserManager.FindAsync(model.Email, model.Password);
        if (user == null)
        {
          ModelState.AddModelError("", "Login or Pass not valid.");
        }
        else
        {
          ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user,
                                  DefaultAuthenticationTypes.ApplicationCookie);
          AuthenticationManager.SignOut();
          AuthenticationManager.SignIn(new AuthenticationProperties
          {
            IsPersistent = true
          }, claim);
          if (String.IsNullOrEmpty(returnUrl))
            return RedirectToAction("Index", "StoreJs");
          return Redirect(returnUrl);
        }
      }
      ViewBag.returnUrl = returnUrl;
      return View(model);
    }
    public ActionResult Logout()
    {
      AuthenticationManager.SignOut();
      return RedirectToAction("Login");
    }
  }
}