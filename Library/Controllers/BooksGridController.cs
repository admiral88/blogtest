﻿using System;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BLL.Services;
using Library.BLL.Models;

namespace Library.Controllers
{
  public class BooksGridController : Controller
  {

    private BookService _bookService = new BookService();

    public ActionResult Index()
    {
      return View();
    }

    public ActionResult Books_Read([DataSourceRequest]DataSourceRequest request)
    {
      var books = _bookService.GetBooks();
      DataSourceResult result = books.ToDataSourceResult(request);
      return Json(result);
    }

    [AcceptVerbs(HttpVerbs.Post)]
    [Authorize(Roles = "admin")]
    public ActionResult Books_Create([DataSourceRequest]DataSourceRequest request, BookViewModel bookViewModel)
    {
      if (ModelState.IsValid)
      {
        _bookService.Create(bookViewModel);
      }
      return Json(new[] { bookViewModel }.ToDataSourceResult(request, ModelState));
    }

    [AcceptVerbs(HttpVerbs.Post)]
    [Authorize(Roles = "admin")]
    public ActionResult Books_Update([DataSourceRequest]DataSourceRequest request, BookViewModel bookViewModel)
    {
      if (ModelState.IsValid)
      {
        _bookService.Update(bookViewModel);
      }
      return Json(new[] { bookViewModel }.ToDataSourceResult(request, ModelState));
    }

    [AcceptVerbs(HttpVerbs.Post)]
    [Authorize(Roles = "admin")]
    public ActionResult Books_Destroy([DataSourceRequest]DataSourceRequest request, BookViewModel book)
    {
      if (ModelState.IsValid)
      {
        _bookService.Delete(book.Id);
      }

      return Json(new[] { book }.ToDataSourceResult(request, ModelState));
    }

    [HttpPost]
    public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
    {
      var fileContents = Convert.FromBase64String(base64);

      return File(fileContents, contentType, fileName);
    }

    [HttpPost]
    public ActionResult Pdf_Export_Save(string contentType, string base64, string fileName)
    {
      var fileContents = Convert.FromBase64String(base64);

      return File(fileContents, contentType, fileName);
    }
  }
}
