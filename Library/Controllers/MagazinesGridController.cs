﻿using System;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BLL.Services;
using Library.BLL.Models;

namespace Library.Controllers
{
  public class MagazinesGridController : Controller
  {
    private MagazineService _magazineService = new MagazineService();

    public ActionResult Index()
    {
      return View();
    }

    public ActionResult Magazines_Read([DataSourceRequest]DataSourceRequest request)
    {
      var magazines = _magazineService.GetMagazines();
      DataSourceResult result = magazines.ToDataSourceResult(request);
      return Json(result);
    }

    [AcceptVerbs(HttpVerbs.Post)]
    public ActionResult Magazines_Create([DataSourceRequest]DataSourceRequest request, MagazineViewModel magazineViewModel)
    {
      if (ModelState.IsValid)
      {
        _magazineService.Create(magazineViewModel);
      }

      return Json(new[] { magazineViewModel }.ToDataSourceResult(request, ModelState));
    }

    [AcceptVerbs(HttpVerbs.Post)]
    public ActionResult Magazines_Update([DataSourceRequest]DataSourceRequest request, MagazineViewModel magazineViewModel)
    {
      if (ModelState.IsValid)
      {
        _magazineService.Update(magazineViewModel);
      }
      return Json(new[] { magazineViewModel }.ToDataSourceResult(request, ModelState));
    }

    [AcceptVerbs(HttpVerbs.Post)]
    public ActionResult Magazines_Destroy([DataSourceRequest]DataSourceRequest request, MagazineViewModel magazineViewModel)
    {
      if (ModelState.IsValid)
      {
        _magazineService.Delete(magazineViewModel.Id);
      }
      return Json(new[] { magazineViewModel }.ToDataSourceResult(request, ModelState));
    }

    [HttpPost]
    public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
    {
      var fileContents = Convert.FromBase64String(base64);

      return File(fileContents, contentType, fileName);
    }

    [HttpPost]
    public ActionResult Pdf_Export_Save(string contentType, string base64, string fileName)
    {
      var fileContents = Convert.FromBase64String(base64);

      return File(fileContents, contentType, fileName);
    }
  }
}
