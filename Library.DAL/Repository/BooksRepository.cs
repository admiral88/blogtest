﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Dapper;
using Library.DAL.Models;

namespace Library.DAL.Repository
{
  public class BooksRepository
  {
    private string _connectionString;

    public BooksRepository()
    {
      _connectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
    }
    
    public List<Book> GetBooks()
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        var books = db.Query<Book>("SELECT * FROM Books").ToList();
        return books;
      }
    }

    public Book Find(int? id)
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        var book = db.QueryFirst<Book>("SELECT * FROM Books WHERE Id = " + id);
        return book;
      }    
    }

    public void Create(Book book)
    {
      StringBuilder sql = new StringBuilder();
      sql.Append("INSERT INTO Books (Author, Name, DateFrom, Genre, Price) ");
      sql.Append("VALUES ( @Author, @Name,@DateFrom, @Genre, @Price)");

      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        db.Query(sql.ToString() , new { Author = book.Author, Name = book.Name, DateFrom = DateTime.Now, Genre = book.Genre, Price = book.Price });
      }   
    }

    public void Update(Book book)
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        db.Query("UPDATE Books SET Author = @Author, Name = @Name, DateFrom = @DateFrom, Genre = @Genre, Price=@Price WHERE Id = @Id",
         new { @Id = book.Id, @Author = book.Author, @Name = book.Name, @DateFrom = book.DateFrom, @Genre = book.Genre, @Price = book.Price });
      }   
    }

    public void Delete(int id)
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        db.Query("DELETE FROM Books WHERE Id = @Id", new { @Id = id });
      }      
    }
  }
}