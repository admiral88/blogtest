﻿using Dapper;
using Library.DAL.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Library.DAL.Repository
{
  public class BrochuresRepository
  {
    private string _connectionString;

    public BrochuresRepository()
    {
      _connectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
    }

    public List<Brochure> GetBrochures()
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        var brochures = db.Query<Brochure>("SELECT * FROM Brochures").ToList();
        return brochures;
      }  
    }

    public Brochure Find(int? id)
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        var brochures = db.QueryFirst<Brochure>("SELECT * FROM Brochures WHERE Id = " + id);
        return brochures;
      }       
    }

    public void Create(Brochure brochures)
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        db.Query("INSERT INTO Brochures ( Name, DateFrom, Genre, Price) VALUES (@Name,@DateFrom, @Genre, @Price)",
                  new { Name = brochures.Name, DateFrom = DateTime.Now, Genre = brochures.Genre, Price = brochures.Price });
      }    
    }

    public void Update(Brochure brochures)
    {
      StringBuilder sql = new StringBuilder();
      sql.Append("UPDATE Brochures SET Name = @Name, DateFrom = @DateFrom, Genre = @Genre, Price=@Price ");
      sql.Append("WHERE Id = @Id");
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        db.Query(sql.ToString(),
        new { @Id = brochures.Id, @Name = brochures.Name, @DateFrom = brochures.DateFrom, @Genre = brochures.Genre, @Price = brochures.Price });
      }
    }

    public void Delete(int id)
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        db.Query("DELETE FROM Brochures WHERE Id = @Id", new { @Id = id });
      }
    }
  }
}