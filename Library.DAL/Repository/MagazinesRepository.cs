﻿using Dapper;
using Library.DAL.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Library.DAL.Repository
{
  public class MagazinesRepository
  {
    private string _connectionString;
    public MagazinesRepository()
    {
      _connectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
    }

    public List<Magazine> GetMagazines()
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        var magazines = db.Query<Magazine>("SELECT * FROM Magazines").ToList();
        return magazines;
      } 
    }

    public Magazine Find(int? id)
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        var magazine = db.QueryFirst<Magazine>("SELECT * FROM Magazines WHERE Id = " + id);
        return magazine;
      }     
    }

    public void Create(Magazine magazine)
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        db.Query("INSERT INTO Magazines ( Name, DateFrom, Genre, Price) VALUES (@Name,@DateFrom, @Genre, @Price)",
         new { Name = magazine.Name, DateFrom = DateTime.Now, Genre = magazine.Genre, Price = magazine.Price });
      }
    }

    public void Update(Magazine magazine)
    {
      StringBuilder sql = new StringBuilder();
      sql.Append("UPDATE Magazines SET Name = @Name, DateFrom = @DateFrom, Genre = @Genre, Price=@Price WHERE Id = @Id");
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        db.Query(sql.ToString(), 
          new { @Id = magazine.Id, @Name = magazine.Name, @DateFrom = magazine.DateFrom, @Genre = magazine.Genre, @Price = magazine.Price });
      }
    }

    public void Delete(int id)
    {
      using (IDbConnection db = new SqlConnection(_connectionString))
      {
        db.Query("DELETE FROM Magazines WHERE Id = @Id", new { @Id = id });
      }
    }
  }
}