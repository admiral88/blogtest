﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace Library.DAL.Models
{
  public class Magazine
  {
    [DisplayName("Id")]
    [ScaffoldColumn(false)]
    public int Id { get; set; }

    [Required]
    [DisplayName("Name")]
    public string Name { get; set; }

    [Required]
    [DisplayName("Date of publication")]
    [DataType(DataType.Date)]
    public DateTime DateFrom { get; set; }

    [Required]
    [DisplayName("Theme")]
    public string Genre { get; set; }

    [Required]
    [DisplayName("Price")]
    [DataType(DataType.Currency)]
    public double Price { get; set; }
  }
}