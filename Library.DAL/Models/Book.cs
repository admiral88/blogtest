﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Library.DAL.Models
{
  [Serializable]
  [XmlRoot("library"), XmlType("library")]
  public class Book
  {
    [DisplayName("Id")]
    [ScaffoldColumn(false)]
    public int Id { get; set; }

    [Required]
    [DisplayName("Author")]
    public string Author { get; set; }

    [Required]
    [DisplayName("Name")]
    public string Name { get; set; }

    [Required]
    [DisplayName("Date of publication")]
    [DataType(DataType.Date)]
    public DateTime DateFrom { get; set; }

    [Required]
    [DisplayName("Genre")]
    public string Genre { get; set; }

    [Required]
    [DisplayName("Price")]
    [DataType(DataType.Currency)]
    public double Price { get; set; }
  }

}