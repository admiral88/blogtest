﻿using AutoMapper;
using Library.BLL.Models;
using Library.DAL.Models;
using Library.DAL.Repository;
using System.Collections.Generic;
using System.Threading;

namespace BLL.Services
{
  public class BookService
  {
    private BooksRepository _booksRepository = new BooksRepository();

    public List<BookViewModel> GetBooks()
    {
      var books = _booksRepository.GetBooks();
      var bookViewModel = Mapper.Map<IEnumerable<Book>, List<BookViewModel>>(books);
      return (bookViewModel);
    }

    public Book Find(int? id)
    {
      var result = _booksRepository.Find(id);
      return (result);
    }

    public void Create(BookViewModel bookViewModel)
    {
      var book = Mapper.Map<BookViewModel, Book>(bookViewModel);
      _booksRepository.Create(book);
    }
    public void Update(BookViewModel bookViewModel)
    {
      var book = Mapper.Map<BookViewModel, Book>(bookViewModel);
      _booksRepository.Update(book);
    }

    public void Delete(int id)
    {
      _booksRepository.Delete(id);
    }

  }
}