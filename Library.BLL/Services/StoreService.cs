﻿using AutoMapper;
using Library.DAL.Repository;
using System.Collections.Generic;
using System;
using Library.DAL.Models;
using Library.BLL.Models;

namespace BLL.Services
{
  public class StoreService
  {
    private BooksRepository _booksRepository;
    private MagazinesRepository _magazinesRepository;
    private BrochuresRepository _brochuresRepository;

    public StoreService()
    {
      _booksRepository = new BooksRepository();
      _magazinesRepository = new MagazinesRepository();
      _brochuresRepository = new BrochuresRepository();
    }

    public List<ViewModel> GetMappedBooks()
    {
      List<ViewModel> mappedBooks = new List<ViewModel>();
      var books = _booksRepository.GetBooks();
      try
      {
        mappedBooks = Mapper.Map<List<Book>, List<ViewModel>>(books);
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
      }
      return mappedBooks;
    }

    public List<ViewModel> GetMappedBrochures()
    {
      var brochures = _brochuresRepository.GetBrochures();
      var mappedBroshures = Mapper.Map<IEnumerable<Brochure>, List<ViewModel>>(brochures);

      return mappedBroshures;
    }
    public List<ViewModel> GetMappedMagazines()
    {
      var magazines = _magazinesRepository.GetMagazines();
      var mappedMagazines = Mapper.Map<IEnumerable<Magazine>, List<ViewModel>>(magazines);

      return mappedMagazines;
    }

    public IEnumerable<ViewModel> GetViewModel()
    {
      var result = GetMappedBooks();
      result.AddRange(GetMappedBrochures());
      result.AddRange(GetMappedMagazines());
      return result;
    }
  }
}