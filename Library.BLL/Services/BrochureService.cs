﻿using AutoMapper;
using Library.BLL.Models;
using Library.DAL.Models;
using Library.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace BLL.Services
{
  public class BrochureService
  {
    BrochuresRepository _brochuresRepository = new BrochuresRepository();

    public List<BrochureViewModel> GetBrochures()
    {
      var brochures = _brochuresRepository.GetBrochures();
      var brochuresViewModel = Mapper.Map<IEnumerable<Brochure>, List<BrochureViewModel>>(brochures);
      return (brochuresViewModel);
    }

    public Brochure Find(int? id)
    {
      return (_brochuresRepository.Find(id));
    }

    public void Create(BrochureViewModel brochuresViewModel)
    {
      var brochure = Mapper.Map<BrochureViewModel, Brochure>(brochuresViewModel);

      _brochuresRepository.Create(brochure);
    }
    public void Update(BrochureViewModel bookViewModel)
    {

      var brochure = Mapper.Map<BrochureViewModel, Brochure>(bookViewModel);

      _brochuresRepository.Update(brochure);
    }

    public void Delete(int id)
    {
      _brochuresRepository.Delete(id);
    }


  }
}