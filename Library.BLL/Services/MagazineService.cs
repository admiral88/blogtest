﻿using Library.DAL.Repository;
using System.Collections.Generic;
using AutoMapper;
using Library.DAL.Models;
using Library.BLL.Models;

namespace BLL.Services
{
  public class MagazineService
  {
    private MagazinesRepository _magazinesRepository = new MagazinesRepository();

    public List<MagazineViewModel> GetMagazines()
    {
      var magazines = _magazinesRepository.GetMagazines();

      var bookViewModel = Mapper.Map<IEnumerable<Magazine>, List<MagazineViewModel>>(magazines);

      return (bookViewModel);
    }

    public Magazine Find(int? id)
    {
      return (_magazinesRepository.Find(id));
    }

    public void Create(MagazineViewModel bookViewModel)
    {

      var magazine = Mapper.Map<MagazineViewModel, Magazine>(bookViewModel);

      _magazinesRepository.Create(magazine);
    }
    public void Update(MagazineViewModel magazineViewModel)
    {

      var magazine = Mapper.Map<MagazineViewModel, Magazine>(magazineViewModel);

      _magazinesRepository.Update(magazine);
    }

    public void Delete(int id)
    {
      _magazinesRepository.Delete(id);
    }
  }
}