﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace Library.BLL.Models
{
  public class ViewModel
  {
    //public IEnumerable<Book> Books { get; set; }

    //public IEnumerable<Magazine> Magazines { get; set; }

    [DisplayName("Id")]
    public int Id { get; set; }

    [DisplayName("Name")]
    public string Name { get; set; }

    [DisplayName("Date of publication")]
    [DataType(DataType.Date)]
    public DateTime DateFrom { get; set; }

    [DisplayName("Theme")]
    public string Theme { get; set; }

    [DisplayName("Price")]
    [DataType(DataType.Currency)]
    public double Price { get; set; }

    [DisplayName("Type")]
    public string TypeProduct { get; set; }
  }
}