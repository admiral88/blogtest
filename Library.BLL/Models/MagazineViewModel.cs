﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.BLL.Models
{
  public class MagazineViewModel
  {
    [DisplayName("Id")]
    [ScaffoldColumn(false)]
    public int Id { get; set; }

    [Required]
    [DisplayName("Name")]
    public string Name { get; set; }

    [Required]
    [DisplayName("Date of publication")]
    [DataType(DataType.Date)]
    public DateTime DateFrom { get; set; }

    [Required]
    [DisplayName("Theme")]
    public string Genre { get; set; }

    [Required]
    [DisplayName("Price")]
    [DataType(DataType.Currency)]
    public double Price { get; set; }
  }
}