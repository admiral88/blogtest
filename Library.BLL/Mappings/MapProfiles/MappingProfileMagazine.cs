﻿using AutoMapper;
using Library.BLL.Models;
using Library.DAL.Models;


namespace Library.BLL.Mappings.MapProfiles
{
  public class MappingProfileMagazine : Profile
  {
    public MappingProfileMagazine()
    {
      CreateMap<MagazineViewModel, Magazine>();
      CreateMap<Magazine, MagazineViewModel>();
    }
  }
}