﻿using AutoMapper;
using Library.BLL.Models;
using Library.DAL.Models;


namespace Library.BLL.Mappings.MapProfiles
{
  public class MappingProfileBrochure : Profile
  {
    public MappingProfileBrochure()
    {
      CreateMap<BrochureViewModel, Brochure>();
      CreateMap<Brochure, BrochureViewModel>();
    }
  }
}