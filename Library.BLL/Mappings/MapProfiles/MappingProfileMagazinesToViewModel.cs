﻿using AutoMapper;
using Library.BLL.Models;
using Library.DAL.Models;
using static Library.DAL.Models.Type;

namespace Library.BLL.Mappings.MapProfiles
{
  public class MappingProfileMagazinesToViewModel : Profile
  {
    public MappingProfileMagazinesToViewModel()
    {
      CreateMap<Magazine, ViewModel>()
        .ForMember("Theme", opt => opt.MapFrom(c => c.Genre))
      .ForMember("TypeProduct", c => c.UseValue(TypeProduct.Magazines.ToString("F")));
    }
  }
}