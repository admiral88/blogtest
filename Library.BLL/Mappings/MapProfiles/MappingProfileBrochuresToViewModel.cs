﻿using AutoMapper;
using Library.BLL.Models;
using Library.DAL.Models;
using static Library.DAL.Models.Type;

namespace Library.BLL.Mappings.MapProfiles
{
  public class MappingProfileBrochuresToViewModel : Profile
  {
    public MappingProfileBrochuresToViewModel()
    {
      TypeProduct typeProduct = TypeProduct.Brochures;

       CreateMap<Brochure, ViewModel>() 
      .ForMember("Theme", opt => opt.MapFrom(c => c.Genre))
      .ForMember("TypeProduct", c => c.UseValue(typeProduct.ToString("F")));;
    }
  }
}