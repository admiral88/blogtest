﻿using AutoMapper;
using Library.BLL.Models;
using Library.DAL.Models;
using System.Collections.Generic;
using static Library.DAL.Models.Type;

namespace Library.BLL.Mappings.MapProfiles
{
  public class MappingProfileBookToViewModel : Profile
  {
    public MappingProfileBookToViewModel()
    {
      CreateMap<Book, ViewModel>()
      .ForMember(c => c.Theme, opt => opt.MapFrom(c => c.Genre))
      .ForMember(c=>c.TypeProduct, opt => opt.UseValue(TypeProduct.Books.ToString("F")));

    }
  }
}