﻿using AutoMapper;
using Library.BLL.Models;
using Library.DAL.Models;


namespace Library.BLL.Mappings.MapProfiles
{
  public class MappingProfileBook : Profile
  {
    public MappingProfileBook()
    {
      CreateMap<Book, BookViewModel>();
      CreateMap<BookViewModel, Book>();
    }
  }
}