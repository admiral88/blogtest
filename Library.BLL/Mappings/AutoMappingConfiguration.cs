﻿using AutoMapper;
using Library.BLL.Mappings.MapProfiles;


namespace Library.BLL.Mappings
{
  public static class AutoMappingConfiguration
  {
    public static void InitializeAutoMapper()
    {
      Mapper.Initialize(cfg =>
      {
        cfg.AddProfile(new MappingProfileBook());
        cfg.AddProfile(new MappingProfileBookToViewModel());
        cfg.AddProfile(new MappingProfileBrochure());
        cfg.AddProfile(new MappingProfileBrochuresToViewModel());
        cfg.AddProfile(new MappingProfileMagazine());
        cfg.AddProfile(new MappingProfileMagazinesToViewModel());
      });

      Mapper.AssertConfigurationIsValid();
    }
  }
}